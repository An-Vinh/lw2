#include <avr/io.h>
#include <util/delay.h> 
#include <avr/interrupt.h>




//-----------------------------Q1-------------------------------
//The LED1 should shine, when the button M2 is pressed. 
/*
int main () { 
DDRB = (1<<PB5);
DDRD &= ~(1<<PD7); 
PORTD |= (1<<PD7);


PCICR|=(1<<PCIE0);
PCMSK0 |=(1<<PCINT0);
sei();

    while(1){

  }

}

ISR(PCINT0_vect){
     PORTB = (1<<5);
}
*/


//------------------------------Q2------------------------------
//The binary number should be displayed on LED8-LED10. The initial value should be
//zero. The value should be increased by one after the button M5 is pressed. The value
//should be cleared when it reach seven (111 in binary). 

/*
int main () { 

DDRC= 0b11111111;

DDRD &= ~(1<<PD3); 
PORTD |= (1<<PD3);


PCICR|=(1<<PCIE2);
PCMSK2 |=(1<<PCINT19);
sei();

    while(1){

  }

}
int x = 1;
ISR(PCINT2_vect){
	int y = x/2;
     PORTC = y;
  	x+=1;
    x = x%14;
  
}*/

//-----------------------------Q3------------------------------



/*
volatile int pressed ;

volatile int i = 1;
int main () { 
DDRB = 0b111111;

int k = 0;
  
DDRD &= ~(1<<PD4); 
PORTD |= (1<<PD4);


PCICR|=(1<<PCIE2);
PCMSK2 |=(1<<PCINT20);
sei();

    while(1){
      while(i){
      	{ 
            // begin f endless while cycle
          if (k == 0 && i){
    PORTB = 0b000100; //turn on the most right led 
            _delay_ms(1000); 
           k+=1;}
         
          if(k==1 && i){
    PORTB = 0b001000; //turn on the 2 most right yellow led
            _delay_ms(1000);
           k+=1;} 
        
          if(k==2 && i){
    PORTB = 0b010000; //turn on the 3 most right yellow led
            _delay_ms(1000);
           k = 0;}
          
 
  
  }
      }
	
  }

}

ISR(PCINT2_vect)
{
  if(PIND & (1<<4)){
  	if(i == 1){
  		i = 0;}
   	else{
    	i = 1;}
  }
}



*/

/*
int main () { 

 DDRC = 0b111111;
DDRD &= ~(1<<PD2); 
PORTD |= (1<<PD2);


PCICR|=(1<<PCIE2);
PCMSK2 |=(1<<PCINT18);
sei();

    while(1){

  }

}

ISR(PCINT2_vect){
     PORTC = 8;
}

*/
